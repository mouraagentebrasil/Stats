<?php

namespace Modules\Stats\Entities;

use Illuminate\Database\Eloquent\Model;

class Stats extends Model
{
    protected $table='stats';
    protected $primaryKey='stats_id';
    protected $fillable=['stats_id','stats_ip','stats_identifier','number_hits','data_ultimo_acesso','service_login_id','fl_convertido','campanha_id','device'];
    public $timestamps = false;

    

    public function Stats()
    {
        return $this->belongsTo('\Modules\Stats\Entities\Stats');
    }

    public function LoginService()
    {
        return $this->belongsTo('\Modules\Stats\Entities\LoginService');
    }
//Não existe esse relacionamento
//    public function Campanha()
//    {
//        return $this->belongsTo('\Modules\Stats\Entities\Campanha');
//    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
