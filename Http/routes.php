<?php

Route::group(['middleware' => ['web','cors'], 'prefix' => 'stats', 'namespace' => 'Modules\Stats\Http\Controllers'], function()
{
    Route::get('/graphs/acessRange','GraphsStatsController@getGraphsAcess');
    //Rotas para stats
    Route::get('/stats', 'StatsController@index');
    Route::post('/stats/storeStats', 'StatsController@storeStats');
    //Route::post('/stats/store', 'StatsController@store');
    //Route::post('/stats/update/{id}','StatsController@update');
    Route::get('/graphs/acessRange/{dateI?}/{dateF?}','GraphsStatsController@getGraphsAcess');


    //[CODE]
});
