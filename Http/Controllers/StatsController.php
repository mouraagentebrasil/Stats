<?php

namespace Modules\Stats\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Stats\Entities\Stats;
use Modules\Stats\Repositories\StatsRepository;

class StatsController extends Controller
{
    
    //retorna item paginados
    public function index()
    {
        $repository = new StatsRepository;
        return $repository->builder()->paginate();
    }

    public function getCampanhaID($campanha){
        $client = new Client();
        try {
            $r = $client->request('GET',getenv('SERVICO_CLIENT')."/campanha/getCampanhaByUrl/".$campanha);
            $data = json_decode($r->getBody()->getContents());
            return $data->campanha_id;
        } catch (\Exception $e){
            return null;
        }

    }

    public function storeStats(Request $request)
    {
        $data = $request->all();
        $data['data_ultimo_acesso'] = date('Y-m-d H:i:s');
        $stats = new Stats();

        $valid = $stats->validate($data,['fl_convertido','number_hits','service_login_id','device']);
        if(is_array($valid)) return response()->json($valid,400);
        $data['campanha_id'] = $this->getCampanhaID($data['campanha_id']);
        $find = $stats->where('stats_ip',$data['stats_ip'])->where('stats_identifier',$data['stats_identifier'])->first();
        if(is_null($find)){
            return $this->store($data);
        }
        return $this->update($data,$find->stats_id);
    }

    //metodo para salvar um novo registro
    public function store($data)
    {
        $repository = new StatsRepository;
        $data['data_ultimo_acesso'] = date('Y-m-d H:i:s');
        $save = $repository->store($data,['fl_convertido','number_hits','service_login_id','device']);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update($data,$id)
    {
        $repository = new StatsRepository;

        $update = $repository->update($id,$data,['fl_convertido','number_hits','service_login_id','device']);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new StatsRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return response()->json($destroy,400);
        return response()->json(['message'=>'Deletado com sucesso'],200);
    }

}