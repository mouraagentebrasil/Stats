<?php
namespace Modules\Stats\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Stats\Entities\Stats;
use Modules\Stats\Repositories\StatsRepository;

class GraphsStatsController extends Controller
{
    public function getGraphsAcess($dateI = null,$dateF = null)
    {

        if(is_null($dateI)){
            $dateI = date('Y-m-d 00:00:00');
        } else {
            $dateI = $dateI." 00:00:00";
        }

        if(is_null($dateF)){
            $dateF = str_replace(' 00:00:00','',$dateI);
            $dateF = $dateF." 23:59:59";
        } else {
            $dateF = $dateF." 23:59:59";
        }
        //var_dump($dateF); exit;
        $model = new Stats();
        $totalAcessos = $model->selectRaw(
            'count(*) hits_unicos,SUM(number_hits) as hits_totais, date_format(data_ultimo_acesso,"%Y-%m-%d") as data_archiv,
             SUM(fl_convertido) as total_conversao'
            )
            ->where('data_ultimo_acesso','>=',$dateI)
            ->where('data_ultimo_acesso','<=',$dateF)
            ->groupBy('data_archiv');
        $data = $totalAcessos->get()->toArray();
        //var_dump($data); exit;

        return $this->formatHighCharts($data,$dateI,$dateF);
    }

    public function formatHighCharts($data,$dateI,$dateF){
        $dateI = date('Y-m-d',strtotime($dateI));
        $dateF = date('Y-m-d',strtotime($dateF));
        $diff = $this->diffDates($dateI,$dateF)+1;
        $return = [];
        //var_dump($data); exit;
        for($d=0;$d<$diff;$d++){
            $strDateFuture = (string) "+".$d." days";
            $data_comp = date('Y-m-d',strtotime($strDateFuture,strtotime($dateI)));
            //var_dump($data[$d]); exit;
            $return[$d] = ['hits_unicos'=>0,'hits_totais'=>0,'data_archiv'=>$data_comp,'total_conversao'=>0];
            for($i=0;$i<count($data);$i++){
                if(!empty($data[$i]) && $data[$i]['data_archiv'] == $data_comp){
                    //die("oi");
                    $return[$d] = $data[$i];
                }
            }

        }
        return $return;
    }

    public function mkTime($date){
        $exp = explode("-",$date);
        $exp2 = explode(" ",$exp[2]);
        $exp3 = explode(":",$exp2[1]);
        $dia = $exp2[0]; $mes=$exp[1]; $ano = $exp[0];
        $hora = $exp3[0]; $minuto = $exp3[1]; $segundo = $exp3[2];
        return mktime($hora,$minuto,$segundo,$mes,$dia,$ano);
    }

    public function diffDates($date1,$date2){
        $time_inicial = strtotime($date1);
        $time_final = strtotime($date2);
        $diferenca = $time_final - $time_inicial;
        $dias = (int)floor( $diferenca / (60 * 60 * 24));
        return $dias;
    }
}